# Strapi application

This repository contians content management system that houses GGEB's website's text and image
content. The process of running Strapi is as follows,

Run the following command (in a console) to start strapi. Then navigate to the URL for admin.

```
$ npm run develop
```

Remember to update the content before you run gatsby. Gatsby only pulls content from strapi once, when
it starts. For further details on Strapi CMS refer to the official documentation.
